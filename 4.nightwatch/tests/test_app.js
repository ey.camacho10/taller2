module.exports = { // adapted from: https://git.io/vodU0
  'Los estudiantes login falied': function(browser) {
	browser
      .url('https://losestudiantes.co/')
      .click('.botonCerrar')
      .waitForElementVisible('.botonIngresar', 4000)
      .click('.botonIngresar')
      .setValue('.cajaLogIn input[name="correo"]', 'wrongemail@example.com')
      .setValue('.cajaLogIn input[name="password"]', '1234')
      .click('.cajaLogIn .logInButton')
      .waitForElementVisible('.aviso.alert.alert-danger', 4000)
      .assert.containsText('.aviso.alert.alert-danger', 'El correo y la contraseña que ingresaste no figuran')
  },
  
  'Find subject, select subject and check result': function(browser) {
	//Punto 3 y 4	
	browser
      .url('https://losestudiantes.co/universidad-de-los-andes/administracion/profesores/ingrid-marcela-leon-diaz')
      .click('.materias input[name="ADMI1590"]')
	  .assert.containsText('.carreraCalificacion', 'Taller De Creatividad')
      .end();
  }
};