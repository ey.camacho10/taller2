var assert = require('assert');
describe('los estudiantes login: ', function() {
	it ('0 should visit los estudiantes and failed at log in', function(){
		//Punto 0
		browser.url('https://losestudiantes.co');
        browser.click('button=Cerrar');
        browser.waitForVisible('button=Ingresar', 2000);
        browser.click('button=Ingresar');
 
        var cajaLogIn = browser.element('.cajaLogIn');
        var mailInput = cajaLogIn.element('input[name="correo"]');

		mailInput.click();
		browser.waitForVisible('input[name="correo"]', 500);
        mailInput.keys('wrongemail@example.com');

        var passwordInput = cajaLogIn.element('input[name="password"]');

		passwordInput.click();
        passwordInput.keys('1234');

        cajaLogIn.element('button=Ingresar').click()
        browser.waitForVisible('.aviso.alert.alert-danger', 3000);

        var alertText = browser.element('.aviso.alert.alert-danger').getText();
        expect(alertText).toBe('Upss! El correo y la contraseña que ingresaste no figuran en la base de datos. Intenta de nuevo por favor.');
	});
	
	it ('1 Register user that exists', function(){
		//Punto 1
		
		//Usuario ya existe
		var cajaSignUp = browser.element('.cajaSignUp');
        var nombreInput = cajaSignUp.element('input[name="nombre"]');
		nombreInput.click();
		nombreInput.keys('Edgar');
		var apellidoInput = cajaSignUp.element('input[name="apellido"]');
		apellidoInput.click();
		apellidoInput.keys('Camacho');		
		var correoInput = cajaSignUp.element('input[name="correo"]');
		correoInput.click();
		correoInput.keys('ey.camacho10@uniandes.edu.co');
		var passwordInput = cajaSignUp.element('input[name="password"]');
		passwordInput.click();
		passwordInput.keys('12345');
		var selectInput = cajaSignUp.element('select[name="idDepartamento"]');
		selectInput.selectByValue("5");
		var aceptaInput = cajaSignUp.element('input[name="acepta"]');
		aceptaInput.click();
		cajaSignUp.element('button=Registrarse').click()
		browser.waitForVisible('.sweet-alert', 3000);
		
		var alertText = browser.element('.sweet-alert').getText();
        expect(alertText).toBe("Ocurrió un error activando tu cuenta\nError: Ya existe un usuario registrado con el correo 'ey.camacho10@uniandes.edu.co'\nOK");
	});
	
	it ('2 Register user', function(){
		//Registrar usuario
		browser.click('button=OK');
		var cajaSignUp = browser.element('.cajaSignUp');
        var nombreInput = cajaSignUp.element('input[name="nombre"]');
		nombreInput.click();
		nombreInput.clearElement();
		nombreInput.keys('Edgar');
		var apellidoInput = cajaSignUp.element('input[name="apellido"]');
		apellidoInput.click();
		apellidoInput.clearElement();
		apellidoInput.keys('Camacho');		
		var correoInput = cajaSignUp.element('input[name="correo"]');
		correoInput.click();
		correoInput.clearElement();
		correoInput.keys('ey.camacho13@uniandes.edu.co');
		var passwordInput = cajaSignUp.element('input[name="password"]');
		passwordInput.click();
		passwordInput.clearElement();
		passwordInput.keys('12345');
		var selectInput = cajaSignUp.element('select[name="idDepartamento"]');
		selectInput.selectByValue("5");
		var aceptaInput = cajaSignUp.element('input[name="acepta"]');
		if (!aceptaInput.isSelected()){
			aceptaInput.click();
		}
		var loginForm = cajaSignUp.element('.loginForm');
		loginForm.element('.logInButton').click();
		browser.waitForVisible('.sweet-alert', 3000);
		
		var alertText = browser.element('.sweet-alert').getText();
        expect(alertText).toContain("Registro exitoso");
		
	});
	
		
	it ('3 Log in user', function(){
		//Login de usuario que se crea
		browser.click('button=OK');
        var cajaLogIn = browser.element('.cajaLogIn');
        var mailInput = cajaLogIn.element('input[name="correo"]');

		mailInput.click();
		browser.waitForVisible('input[name="correo"]', 500);
        mailInput.keys('wrongemail@example.com');

        var passwordInput = cajaLogIn.element('input[name="password"]');

		passwordInput.click();
        passwordInput.keys('1234');

        cajaLogIn.element('.logInButton').click();
        browser.waitForVisible('.aviso.alert.alert-danger', 3000);

		var alertText = browser.element('input[name="cuenta"]').selector;        
        expect(alertText).toBe('input[name="cuenta"]');
	});
	
	it ('4 should find a teacher', function(){
		//Punto 2
		var inputPlace = browser.element(".Select-placeholder");
		inputPlace.click();
		browser.waitForVisible('.Select-placeholder', 2000);
		inputPlace.keys("ingr");
		var alertText = browser.element(".descripcionProfesor").getText();
		expect(alertText).toBe("Ingrid");
	});
});