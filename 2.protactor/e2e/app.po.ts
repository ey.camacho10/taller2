import {browser, by, element, ElementFinder} from 'protractor';

export class TourOfHeroesPage {
  navigateTo() {
    return browser.get('/');
  }

  getTop4Heroes() {
    return element.all(by.css('.module.hero')).all(by.tagName('h4')).getText();
  }

  navigateToHeroes() {
    element(by.linkText('Heroes')).click();
  }

  getAllHeroes() {
    return element(by.tagName('my-heroes')).all(by.tagName('li'));
  }

  enterNewHeroInInput(newHero: string) {
    element(by.tagName('input')).sendKeys(newHero);
    element(by.buttonText('Add')).click();
  }

  //Punto 1
  searchHeroe(heroe: string) {
    element(by.tagName('input')).sendKeys(heroe);
    return element.all(by.css(".search-result")).getText();
  }

  //Punto 2
  deleteHeroe(heroe: string) {
    var elemento = this.getElementHeroe(heroe);
    elemento.element(by.tagName('button')).click();
  }

  //Punto 3
  updateHero(heroe:string, newHero: string){
    var elemento = this.getElementHeroe(heroe);
    elemento.click();
    element(by.buttonText('View Details')).click();
    element(by.tagName('input')).clear();
    element(by.tagName('input')).sendKeys(newHero);
    element(by.buttonText('Save')).click();
    this.navigateToHeroes();
    elemento = this.getElementHeroe(newHero);
    return elemento.all(by.tagName('span')).last().getText();
  }

  //Punto 4
  navigateHeroDashToHero() {
      element.all(by.css('.col-1-4')).first().click();
      return element(by.buttonText('Save')).getText();
  }

  //Punto 5
  navigateHeroListToHero(heroe: string) {
      var elemento = this.getElementHeroe(heroe);
      elemento.click();
      element(by.buttonText('View Details')).click();
      return element(by.buttonText('Save')).getText();
  }

  //Punto 6
  navigateHeroSearchToHero(heroe: string){
    element(by.tagName('input')).sendKeys(heroe);
    element.all(by.css(".search-result")).first().click();
    return element(by.tagName('input')).getAttribute("value");
  }

  getElementHeroe(heroe: string){
    var elemento =
    element(by.css('.heroes')).all(by.tagName('li')).filter(
      function(elem, index) {
        return elem.getText().then(function (text){
              return text.indexOf(heroe) !== -1;
        });
    }).first();
    return elemento;
  }
}
