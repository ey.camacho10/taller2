import { TourOfHeroesPage } from './app.po';

describe('Tour of heroes Dashboard', () => {
  let page: TourOfHeroesPage;

  beforeEach(() => {
    page = new TourOfHeroesPage();
  });

  it('should display top 4 heroes', () => {
    page.navigateTo();
    expect(page.getTop4Heroes()).toEqual(['Mr. Nice', 'Narco', 'Bombasto', 'Celeritas']);
  });

  it('should navigate to heroes', () => {
    page.navigateToHeroes();
    expect(page.getAllHeroes().count()).toBe(11);
  });

  //Punto 1
  it('should search a hero', () => {
    page.navigateTo();
    expect(page.searchHeroe('n')).toContain('RubberMan');
  });

  //Punto 4
  it('should navigate a hero from dashboard to heroes', () => {
    page.navigateTo();
    expect(page.navigateHeroDashToHero()).toEqual('Save');
  });

  //Punto 6
  it('should navigate from hero search to heroe detail', () => {
    page.navigateTo();
    expect(page.navigateHeroSearchToHero("zer")).toEqual('Zero');
  });
});

describe('Tour of heroes, heroes page', () => {
  let page: TourOfHeroesPage;

  beforeEach(() => {
    page = new TourOfHeroesPage;
    page.navigateToHeroes();
  });

  it('should add a new hero', () => {
    const currentHeroes = page.getAllHeroes().count();
    page.enterNewHeroInInput('My new Hero');
    expect(page.getAllHeroes().count()).toBe(currentHeroes.then(n => n + 1));
  });

  //Punto 2
  it('should delete a hero', () => {
    const currentHeroes = page.getAllHeroes().count();
    page.deleteHeroe('Dynama');
    expect(page.getAllHeroes().count()).toBe(currentHeroes.then(n => n - 1));
  });

  //Punto 5
  it('should navigate from heroes list to hero', () => {
    page.navigateToHeroes();
    expect(page.navigateHeroListToHero('Magneta')).toEqual('Save');
  });

  //Punto 3
  it('should update a hero', () => {
    expect(page.updateHero('RubberMan','nuevoHero')).toEqual('nuevoHero');
  });
});
